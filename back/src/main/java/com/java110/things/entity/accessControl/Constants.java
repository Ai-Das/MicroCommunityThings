package com.java110.things.entity.accessControl;

public interface Constants {

    interface Version {
        int V1 = 1;
    }



    enum DeviceCmd {
        ADD_FACE, // 创建人脸

        OPEN_DOOR, // 开门

        REBOOT,// 重启设备

        UPDATE_FACE, //修改人脸

        DELETE_FACE, //删除人脸

        UI_TITLE,// 设置名称

        FACE_SEARCH;// 搜素设备
    }

    enum UserType {
        OWNER,
        VISITOR,
        BLACKLIST;
    }
}
