package com.java110.things.service.accessControl.iFaceTermMqtt;

import java.io.Serializable;

public class BaseMQTTMsg implements Serializable {
    private String taskId;

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }
}
