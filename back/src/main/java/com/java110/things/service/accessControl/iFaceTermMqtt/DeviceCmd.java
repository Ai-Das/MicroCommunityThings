package com.java110.things.service.accessControl.iFaceTermMqtt;

import org.omg.CORBA.UNKNOWN;

public enum DeviceCmd {
    REBOOT,
    OPEN_DOOR,
    CLEAR_CACHE,
    ONLINE,
    OFFLINE,
    OPEN_DOOR_LOG,
    UNKNOWN;

    public static DeviceCmd getCmd(String cmd) {
        try {
            return valueOf(cmd);
        } catch (Exception e) {
            return UNKNOWN;
        }
    }
}
