package com.java110.things.service.accessControl.iFaceTermMqtt;

public enum ConfigCmd {
    MODIFY_TITLE,
    HIDE_INFO,
    MODIFY_PWD,
    FACE_PARAM,
    UNKNOWN;

    public static ConfigCmd getCmd(String cmd) {
        try {
            return valueOf(cmd);
        } catch (Exception e) {
            return UNKNOWN;
        }
    }
}
