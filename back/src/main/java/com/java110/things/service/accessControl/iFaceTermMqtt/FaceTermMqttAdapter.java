package com.java110.things.service.accessControl.iFaceTermMqtt;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.java110.things.constant.ResponseConstant;
import com.java110.things.entity.accessControl.*;
import com.java110.things.entity.cloud.MachineCmdResultDto;
import com.java110.things.entity.fee.FeeDto;
import com.java110.things.entity.machine.MachineDto;
import com.java110.things.entity.machine.OperateLogDto;
import com.java110.things.entity.openDoor.OpenDoorDto;
import com.java110.things.entity.response.ResultDto;
import com.java110.things.entity.room.RoomDto;
import com.java110.things.factory.MqttFactory;
import com.java110.things.factory.NotifyAccessControlFactory;
import com.java110.things.service.accessControl.IAssessControlProcess;
import com.java110.things.service.accessControl.ICallAccessControlService;
import com.java110.things.service.machine.IMachineService;
import com.java110.things.util.SeqUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service("FaceTermMqttAdapter")
public class FaceTermMqttAdapter implements IAssessControlProcess {


    private static Logger logger = LoggerFactory.getLogger(FaceTermMqttAdapter.class);

    @Autowired
    private IMachineService machineServiceImpl;

    public static final String OPEN_TYPE_FACE = "1000"; // 人脸开门

    public static final String PRODUCT_NAME = "iFaceTerm_";

    //单设备处理
    public static final String TOPIC_FACE_SN_REQUEST = "iFaceTerm/%s/V1/%s/%s";

    //识别结果上报
    public static final String TOPIC_FACE_RESPONSE = "RESPONSE/iFaceTerm/V1/DEVICE/OPEN_DOOR_LOG";

    //硬件上线上报
    public static final String TOPIC_ONLINE_RESPONSE = "RESPONSE/iFaceTerm/V1/DEVICE/ONLINE";

    //硬件上线上报
    public static final String TOPIC_CMD_RESPONSE = "RESPONSE/iFaceTerm/V1";
    public static final String SN = "{sn}";


    public static final String FACE_RESULT = "face_result";


    @Override
    public void initAssessControlProcess() {
        //注册设备上线 topic
        MqttFactory.subscribe(TOPIC_ONLINE_RESPONSE);
        //注册设备响应 topic
        MqttFactory.subscribe(TOPIC_CMD_RESPONSE);
        //注册设备上报人脸记录 topic
        MqttFactory.subscribe(TOPIC_FACE_RESPONSE);
    }

    @Override
    public int getFaceNum(MachineDto machineDto) {
        return 0;
    }

    @Override
    public String getFace(MachineDto machineDto, UserFaceDto userFaceDto) {
        return null;
    }

    @Override
    public void addFace(MachineDto machineDto, UserFaceDto userFaceDto) {
        String cmdId = SeqUtil.getId();
        FaceInfo faceInfo = new FaceInfo();
        faceInfo.setTaskId(userFaceDto.getTaskId());
        faceInfo.setUserId(userFaceDto.getUserId());
        faceInfo.setUserName(userFaceDto.getName());
        faceInfo.setIdNumber(userFaceDto.getIdNumber());
        faceInfo.setStartTime(userFaceDto.getStartTime());
        faceInfo.setEndTime(userFaceDto.getEndTime());
        faceInfo.setUsrType(Constants.UserType.OWNER.ordinal());
        String payload = JSON.toJSONString(faceInfo);

        String topic = String.format(TOPIC_FACE_SN_REQUEST, machineDto.getMachineCode(), CmdType.FACE, FaceCmd.ADD);

        MqttFactory.publish(topic, payload);
        saveLog(cmdId,
                machineDto.getMachineId(),
                topic,
                payload,
                "", "",
                userFaceDto.getUserId(),
                userFaceDto.getName());

    }

    @Override
    public void updateFace(MachineDto machineDto, UserFaceDto userFaceDto) {
        String cmdId = SeqUtil.getId();
        FaceInfo faceInfo = new FaceInfo();
        faceInfo.setTaskId(userFaceDto.getTaskId());
        faceInfo.setUserId(userFaceDto.getUserId());
        faceInfo.setUserName(userFaceDto.getName());
        faceInfo.setIdNumber(userFaceDto.getIdNumber());
        faceInfo.setStartTime(userFaceDto.getStartTime());
        faceInfo.setEndTime(userFaceDto.getEndTime());
        faceInfo.setUsrType(Constants.UserType.OWNER.ordinal());
        String payload = JSON.toJSONString(faceInfo);
        String topic = String.format(TOPIC_FACE_SN_REQUEST, machineDto.getMachineCode(), CmdType.FACE, FaceCmd.UPDATE);

        MqttFactory.publish(topic, payload);
        saveLog(cmdId,
                machineDto.getMachineId(),
                topic,
                payload,
                "", "",
                userFaceDto.getUserId(),
                userFaceDto.getName());

    }

    @Override
    public void deleteFace(MachineDto machineDto, HeartbeatTaskDto heartbeatTaskDto) {
        String cmdId = SeqUtil.getId();

        DeleteFaceDto deleteFaceCmdDto = new DeleteFaceDto();
        deleteFaceCmdDto.setTaskId(heartbeatTaskDto.getTaskid());
        deleteFaceCmdDto.setUserId(heartbeatTaskDto.getTaskinfo());
        String payload = JSON.toJSONString(deleteFaceCmdDto);
        String topic = String.format(TOPIC_FACE_SN_REQUEST, machineDto.getMachineCode(), CmdType.FACE, FaceCmd.DELETE);

        MqttFactory.publish(topic, payload);
        saveLog(cmdId,
                machineDto.getMachineId(),
                topic,
                payload,
                "",
                "",
                heartbeatTaskDto.getTaskinfo(),
                "");

    }

    @Override
    public void clearFace(MachineDto machineDto, HeartbeatTaskDto heartbeatTaskDto) {
        String cmdId = SeqUtil.getId();

        BaseMQTTMsg clearFaceCmdDto = new BaseMQTTMsg();
        clearFaceCmdDto.setTaskId(heartbeatTaskDto.getTaskid());
        String payload = JSON.toJSONString(clearFaceCmdDto);
        String topic = String.format(TOPIC_FACE_SN_REQUEST, machineDto.getMachineCode(), CmdType.FACE, FaceCmd.DELETE);
        MqttFactory.publish(topic, payload);

        saveLog(cmdId, machineDto.getMachineId(), topic, payload, "");

    }

    @Override
    public List<MachineDto> scanMachine() throws Exception {
        return null;
    }

    @Override
    public void mqttMessageArrived(String topic, String data) {
        JSONObject param = JSONObject.parseObject(data);

        switch (topic) {
            case TOPIC_FACE_RESPONSE:
                openDoorResult(data);
                break;
            case TOPIC_ONLINE_RESPONSE: //硬件上线
                machineOnline(data);
                break;
            default:
                machineCmdResult(data);
                break;
        }

        if (!param.containsKey("cmd_id")) {
            return;
        }
        String state = param.containsKey("code") && "0".equals(param.getString("code")) ? "10002" : "10003";
        String marchineId = "-1";
        if (param.containsKey("sn")) {
            ResultDto resultDto = null;
            MachineDto machineDto = new MachineDto();
            machineDto.setMachineCode(param.getString("sn"));
            machineDto.setMachineTypeCd("9998"); // 标识门禁 以防万一和道闸之类冲突
            try {
                resultDto = machineServiceImpl.getMachine(machineDto);
                if (resultDto != null && resultDto.getCode() == ResponseConstant.SUCCESS) {
                    List<MachineDto> machineDtos = (List<MachineDto>) resultDto.getData();
                    if (machineDtos.size() > 0) {
                        marchineId = machineDtos.get(0).getMachineId();
                    }
                }
            } catch (Exception e) {

            }


        }
        saveLog(param.getString("cmd_id"), marchineId, topic, "", data, state);
    }

    /**
     * 开门记录
     *
     * @param data 设备推送结果
     *             {
     *             "type" : "face_result" ,
     *             "body" : {
     *             "e_imgurl" : "http://jupiter-1251895221.cos.ap-guangzhou.myqcloud.
     *             com/20190907/8080078b-c30d4f7b_1567816001_95_101.jpg",
     *             //人脸图url（若识别后不存在全图数据以及上传img_data失败，不推送此字段）
     *             "e_imgsize" : 159792, //人脸图大小
     *             "hat" : 255,
     *             "matched" : 95, //比对结果(100分制)，0：未比对。-1：比对失败。大于0的取值
     *             "name" : "k", //人员姓名
     *             "per_id" : "20190906135824899",
     *             "role" : 1, //人员角色，0：普通人员。 1：白名单人员。 2：黑名单人员
     *             "usec" : 1567816001,
     *             "sn" : "ffffffff" //设备的SN信息
     *             }
     *             }
     */
    private void openDoorResult(String data) {
        ICallAccessControlService notifyAccessControlService = NotifyAccessControlFactory.getCallAccessControlService();
        try {
            JSONObject param = JSONObject.parseObject(data);

            if (param.containsKey("type") && !FACE_RESULT.equals(param.getString("type"))) {
                return;
            }

            JSONObject body = param.getJSONObject("body");
            OpenDoorDto openDoorDto = new OpenDoorDto();
            openDoorDto.setFace(body.getString("face_imgdata"));
            openDoorDto.setUserName(body.containsKey("name") ? body.getString("name") : "");
            openDoorDto.setHat(body.getString("hat"));
            openDoorDto.setMachineCode(body.getString("sn"));
            openDoorDto.setUserId(body.containsKey("per_id") ? body.getString("per_id") : "");
            openDoorDto.setOpenId(SeqUtil.getId());
            openDoorDto.setOpenTypeCd(OPEN_TYPE_FACE);
            openDoorDto.setSimilarity(body.containsKey("matched") ? body.getString("matched") : "0");


            freshOwnerFee(openDoorDto);

            notifyAccessControlService.saveFaceResult(openDoorDto);

        } catch (Exception e) {
            logger.error("推送人脸失败", e);
        }

    }

    /**
     * 查询费用信息
     *
     * @param openDoorDto
     */
    private void freshOwnerFee(OpenDoorDto openDoorDto) {

        ICallAccessControlService notifyAccessControlService = NotifyAccessControlFactory.getCallAccessControlService();
        List<FeeDto> feeDtos = new ArrayList<>();
        try {
            //查询业主房屋信息
            UserFaceDto userFaceDto = new UserFaceDto();
            userFaceDto.setUserId(openDoorDto.getUserId());
            List<RoomDto> roomDtos = notifyAccessControlService.getRooms(userFaceDto);

            if (roomDtos == null || roomDtos.size() < 1) {
                return;
            }

            for (RoomDto roomDto : roomDtos) {
                List<FeeDto> tmpFeeDtos = notifyAccessControlService.getFees(roomDto);
                if (tmpFeeDtos == null || tmpFeeDtos.size() < 1) {
                    continue;
                }
                feeDtos.addAll(tmpFeeDtos);
            }
        } catch (Exception e) {
            logger.error("云端查询物业费失败", e);
        }

        if (feeDtos.size() < 1) {
            openDoorDto.setAmountOwed("0");
            return;
        }
        double own = 0.00;
        for (FeeDto feeDto : feeDtos) {
            logger.debug("查询费用信息" + JSONObject.toJSONString(feeDto));
            own += feeDto.getAmountOwed();
        }

        openDoorDto.setAmountOwed(own + "");
    }


    /**
     * 设备上线
     *
     * @param data {
     *             "cmd": "mqtt_online",
     *             "sn": "fffffff",
     *             "result": "mqtt is online"
     *             }
     */
    private void machineOnline(String data) {
        JSONObject param = JSONObject.parseObject(data);

        String machineCode = param.getString("machineCode");
        MachineDto machineDto = new MachineDto();
        machineDto.setMachineCode(machineCode);
        machineDto.setMachineMac(machineCode);
        ResultDto resultDto = null;

        try {
            resultDto = machineServiceImpl.getMachine(machineDto);
        } catch (Exception e) {
            logger.error("查询设备失败", machineDto);
        }

        if (resultDto == null || resultDto.getCode() != ResponseConstant.SUCCESS) {
            logger.error("查询设备信息失败" + machineDto.toString());
            return;
        }

        List<MachineDto> machineDtos = (List<MachineDto>) resultDto.getData();

        if (machineDtos.size() > 0) {
            logger.debug("门禁已经注册过，无需再次注册");
            return;
        }
        String machineName = PRODUCT_NAME + SeqUtil.getMachineSeq();

        try {
            //设备上报
            ICallAccessControlService notifyAccessControlService = NotifyAccessControlFactory.getCallAccessControlService();
            machineDto = new MachineDto();
            machineDto.setMachineId(UUID.randomUUID().toString());
            machineDto.setMachineIp("设备未上报");
            machineDto.setMachineMac(machineCode);
            machineDto.setMachineCode(machineCode);
            machineDto.setMachineName(machineName);
            machineDto.setMachineVersion("v1.0");
            machineDto.setOem("艾达斯");
            notifyAccessControlService.uploadMachine(machineDto);
        } catch (Exception e) {
            logger.error("上报设备失败", e);
        }


    }


    private void machineCmdResult(String data) {
        JSONObject resultCmd = JSONObject.parseObject(data);
        if (!resultCmd.containsKey("cmd")) {
            return;
        }
        Constants.DeviceCmd cmd = Constants.DeviceCmd.valueOf(resultCmd.getString("cmd"));
        switch (cmd) {
            case ADD_FACE:
            case UPDATE_FACE:
                doCmdResultCloud(resultCmd);
                break;
            case DELETE_FACE:
                break;
            default:
                break;
        }
    }


    private void doCmdResultCloud(JSONObject resultCmd) {
        try {
            String taskId = resultCmd.getString("cmdId");
            String machineCode = resultCmd.getString("sn");
            int code = -1;
            if (!resultCmd.containsKey("code")) {
                code = -1;
            } else {
                code = resultCmd.getIntValue("code") != 0 ? -1 : 0;
            }
            String msg = resultCmd.containsKey("reply") ? resultCmd.getString("reply") : "";
            ICallAccessControlService notifyAccessControlService = NotifyAccessControlFactory.getCallAccessControlService();
            MachineCmdResultDto machineCmdResultDto = new MachineCmdResultDto(code, msg, taskId, machineCode);
            notifyAccessControlService.machineCmdResult(machineCmdResultDto);
        } catch (Exception e) {
            logger.error("上报执行命令失败", e);
        }
    }

    @Override
    public void restartMachine(MachineDto machineDto) {
        String cmdId = SeqUtil.getId();
        BaseMQTTMsg rebootCmdDto = new BaseMQTTMsg();
        rebootCmdDto.setTaskId(cmdId);
        String payload = JSON.toJSONString(rebootCmdDto);
        String topic = String.format(TOPIC_FACE_SN_REQUEST, machineDto.getMachineCode(), CmdType.DEVICE, DeviceCmd.REBOOT);
        MqttFactory.publish(topic, payload);
        saveLog(cmdId, machineDto.getMachineId(), topic, payload, "");

    }

    @Override
    public void openDoor(MachineDto machineDto) {
        String cmdId = SeqUtil.getId();
        BaseMQTTMsg openDoorDto = new BaseMQTTMsg();
        openDoorDto.setTaskId(cmdId);
        String payload = JSON.toJSONString(openDoorDto);
        String topic = String.format(TOPIC_FACE_SN_REQUEST, machineDto.getMachineCode(), CmdType.DEVICE, DeviceCmd.OPEN_DOOR);
        MqttFactory.publish(topic, payload);
        saveLog(cmdId, machineDto.getMachineId(), topic, payload, "");
    }

    @Override
    public boolean httpFaceResult(String data) {
        return false;
    }

    /**
     * 存储日志
     *
     * @param logId     日志ID
     * @param machineId 设备ID
     * @param cmd       操作命令
     * @param reqParam  请求报文
     * @param resParam  返回报文
     */
    private void saveLog(String logId, String machineId, String cmd, String reqParam, String resParam) {
        saveLog(logId, machineId, cmd, reqParam, resParam, "", "", "");
    }

    /**
     * 存储日志
     *
     * @param logId     日志ID
     * @param machineId 设备ID
     * @param cmd       操作命令
     * @param reqParam  请求报文
     * @param resParam  返回报文
     * @param state     状态
     */
    private void saveLog(String logId, String machineId, String cmd, String reqParam, String resParam, String state) {
        saveLog(logId, machineId, cmd, reqParam, resParam, state, "", "");
    }


    /**
     * 存储日志
     *
     * @param logId     日志ID
     * @param machineId 设备ID
     * @param cmd       操作命令
     * @param reqParam  请求报文
     * @param resParam  返回报文
     * @param state     状态
     * @param userId    业主ID
     * @param userName  业主名称
     */
    private void saveLog(String logId, String machineId, String cmd, String reqParam, String resParam, String state, String userId, String userName) {
        ICallAccessControlService notifyAccessControlService = NotifyAccessControlFactory.getCallAccessControlService();
        OperateLogDto operateLogDto = new OperateLogDto();
        operateLogDto.setLogId(logId);
        operateLogDto.setMachineId(machineId);
        operateLogDto.setOperateType(cmd);
        operateLogDto.setReqParam(reqParam);
        operateLogDto.setResParam(resParam);
        operateLogDto.setState(state);
        operateLogDto.setUserId(userId);
        operateLogDto.setUserName(userName);
        notifyAccessControlService.saveOrUpdateOperateLog(operateLogDto);
    }
}
