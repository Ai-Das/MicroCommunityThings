package com.java110.things.service.accessControl.iFaceTermMqtt;

public class DeleteFaceDto extends BaseMQTTMsg {

    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}
