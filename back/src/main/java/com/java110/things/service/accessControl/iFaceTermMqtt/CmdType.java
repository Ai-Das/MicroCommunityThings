package com.java110.things.service.accessControl.iFaceTermMqtt;

public enum CmdType {
    FACE,
    DEVICE,
    CONFIG,
    UNKNOWN;

    public static CmdType getCmd(String cmd) {
        try {
            return valueOf(cmd);
        } catch (Exception e) {
            return UNKNOWN;
        }
    }
}
