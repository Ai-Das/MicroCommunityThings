package com.java110.things.service.accessControl.iFaceTermMqtt;

public enum FaceCmd {
    ADD,
    UPDATE,
    DELETE,
    CLEAR,
    UNKNOWN;

    public static FaceCmd getCmd(String cmd) {
        try {
            return valueOf(cmd);
        } catch (Exception e) {
            return UNKNOWN;
        }
    }
}
